// TODO: will be deprecated.

import { cache } from 'react'
import 'server-only'

async function request(url, params, method = 'GET') {

  let options = {
    method,
    // headers: {
    //   'Content-Type': 'application/json'
    // }
  };

  if (params) {
    if (method === 'GET') {
      url += '?' + objectToQueryString(params);

    } 
    // else {
    //     options = {
    //       ...options,
    //       body :JSON.stringify(params)
    //     }

    // }
    
  }

  console.log("TEST URL HIT : ", process.env.URL_API + url + `api_key=${process.env.API_KEY}`)

  const response = await fetch(process.env.URL_API + url + `api_key=${process.env.API_KEY}`, options); 
  
  console.log("RESPONSE : ", response)

  if (response.status >= 200 && response.status < 300) {
    return generateErrorResponse('The server responded with an unexpected status.');
  }

  const result = await response.json();

  console.log("RESPONSE result : ", result)


  return result;

}

function objectToQueryString(obj) {
  return Object.keys(obj).map(key => key + '=' + obj[key]).join('&');
}

function generateErrorResponse(message) {
  return {
    status : 'error',
    message
  };
}

function get(url, params) {
  return request(url, params);
}

function create(url, params) {
  return request(url, params, 'POST');
}

 function update(url, params) {
  return request(url, params, 'PUT');
}

function remove(url, params) {
  return request(url, params, 'DELETE');
}

export default {
  get,
  create,
  update,
  remove
};


