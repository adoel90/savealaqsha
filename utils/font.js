import { Megrim} from 'next/font/google'

const fontMegrim = Megrim({subsets: ['latin'],weight: '400'})

export { fontMegrim }
