const headerNavLinks = [
  { href: '/', title: 'Beranda' },
  { href: '/blog', title: 'Serpihan Berita' },
  { href: '/history', title: 'Serpihan Sejarah' },
  // { href: '/blog', title: 'Aktifis' },
  { href: '/about', title: 'Tentang Kami' },
  // { href: '/tags', title: 'Tags' },
  // { href: '/projects', title: 'Pembakar Semangat' },
  { href: '/auth/contributor', title: 'As Contributor' },
]

export default headerNavLinks
