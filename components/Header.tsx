'use client'
import { useRef, useEffect } from 'react';
import siteMetadata from '@/data/siteMetadata'
import headerNavLinks from '@/data/headerNavLinks'
import Link from './Link'
import MobileNav from './MobileNav'
import ThemeSwitch from './ThemeSwitch'
import SearchButton from './SearchButton'
import { fontMegrim } from '@/utils/font';
import { useInView } from 'framer-motion';
import { getProfile } from '@/services/auth';


const Header = () => {

  const refLogo = useRef(null);
  const isInViewLogo = useInView(refLogo, {
    once: true,
  });

  const actionGetProfile = async () => {

    await getProfile();    
  }

  useEffect(() => {

    actionGetProfile();
    
  },[])


  return (
    <header className="flex items-center justify-between py-10">
      <div>
        <Link href="/" aria-label={siteMetadata.headerTitle}>
          <div className="flex items-center justify-between">                       
            <h5
              ref={refLogo}
              className={`h-6 text-2xl text-gray-900 dark:text-gray-100 font-semibold ${fontMegrim.className}`}
              style={{
                transform: isInViewLogo ? 'none' : 'translateX(17px)',
                opacity: isInViewLogo ? 1 : 0,
                transition: 'all 0.9s cubic-bezier(0.17, 0.55, 0.55, 1) 0.5s',
              }}
            >
              Save Al-Aqsha
            </h5>
            </div>
        </Link>
      </div>
      <div className="flex items-center space-x-4 leading-5 sm:space-x-6">
        {headerNavLinks
          .filter((link) => link.href !== '/')
          .map((link) => (
            <Link
              key={link.title}
              href={link.href}
              className="hidden font-medium text-gray-900 dark:text-gray-100 sm:block"
            >
              {link.title}
            </Link>
          ))}
        <SearchButton />
        <ThemeSwitch />
        <MobileNav />
      </div>
    </header>
  )
}

export default Header
