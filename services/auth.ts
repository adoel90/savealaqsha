
"use server"
import { cache } from 'react'
import { redirect } from 'next/navigation'
import { cookies } from 'next/headers'

const TEXT_SUCCESS = "success";

const isVerifyToken = cache(() => {

    const cookiesList = cookies()
    const hasToken = cookiesList.has('token')
    return hasToken;

})

const login = cache(async (data) => {

    const { username, password } = data;
    const res = await fetch(`${process.env.URL_API}/users/login`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json' 
        },
        body: JSON.stringify({ username, password}) 
    }); 

    let response = await res.json();

    if(response?.status === TEXT_SUCCESS){

        console.log("RESPONSE : ", response)
        const token = response.data.token;

        cookies().set('token',token,{ secure: true})
        redirect("/");            
    }
                    
    return await res.json() ;
        
});

const getProfile = cache(async () => {
    
    if(isVerifyToken()){
        const cookiesList = cookies()
        const cookiesToken = cookiesList.get('token')

        console.log("cookiesToken : ", cookiesToken)
    }


})

    

// const getProducts = cache(async (param) => {

//     const res = await fetch(`${process.env.URL_PLATZI_STORE_API}/products?offset=${param?.offset ?? 0}&limit=${param?.limit ?? 10}`);    
//     return await res.json() ;
  
// });

// const getProductDetail = cache(async (id) => {

//     const res = await fetch(`${process.env.URL_PLATZI_STORE_API}/products/${id}`);        
//     return await res.json() ;
  
// });


// const createProduct = cache(async (data) => {
    
//     const { title, price } = data;
//     const res = await fetch(`${process.env.URL_PLATZI_STORE_API}/products`, {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json' 
//         },
//         body: JSON.stringify({
//             title: title ?? "",
//             price: price ?? 0,
//             description: "Test Description",
//             categoryId: 1,
//             images: ["https://placeimg.com/640/480/any"]
//         }) 
//     });        

//     return await res.json() ;
  
// });


// const updateProduct = cache(async (id, data) => {
    
//     const { title, price } = data;
//     const res = await fetch(`${process.env.URL_PLATZI_STORE_API}/products/${id}`, {
//         method: 'PUT',
//         headers: {
//           'Content-Type': 'application/json' 
//         },
//         body: JSON.stringify({
//             title: title ?? "",
//             price: price ?? 0          
//         }) 
//     });        

//     return await res.json() ;
  
// });


export { 
    login,
    getProfile
    // getProducts, 
    // getProductDetail, 
    // createProduct, 
    // updateProduct 

}

