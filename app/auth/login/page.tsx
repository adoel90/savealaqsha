"use client";
import { useForm } from "react-hook-form";
import { login } from "@/services/auth";
// @ts-ignore
// import { experimental_useFormStatus as useFormStatus } from "react-dom"


// const initialState = {
//     message: ""
// }

export default function Login(){

    const {  register, handleSubmit, formState } = useForm({
      defaultValues: {
        username: '',
        password: ''
      }
    })  

    //    
    // const { pending } = useFormStatus();
    const { isLoading } = formState;

    const actionSubmitLogin = handleSubmit(async (params) => {  
                
        let response = null;

        response = await login(params);
        
        if(response){
            console.warn(response)
        };

    })

    // const actionSubmitProduct = handleSubmit(async (params) => {                  
      
    //   let response = null;
    //   if(isEdit){

    //     response = await updateProduct(data?.id, params);          

    //     if(response){
    //       toast.success("Update successfully")
    //       setTimeout(() => {
    //         goBack();
    //       },2000)
    //     }
    //     return;
    //   }

    //    response = await createProduct(params);  

    //     if(response){
    //       toast.success("Add Product successfully")
    //       setTimeout(() => {
    //         goBack();
    //       },2000)
    //     }

    // });



    return (
        <section className="grid place-items-center overflow-hidden h-screen">
            {/* <form action={formAction} className="flex flex-col items-left">            
                <label className="text-left">Email</label>
                <input type="email" className="rounded bg-['rgba(255, 255, 255, 0.06)']" name="email" />

                <label className="text-left mt-5">Password</label>
                <input type="password" className="rounded bg-['rgba(255, 255, 255, 0.06)']" name="password" />
                <p aria-live="polite" className="sr-only" role="status">
                    {state?.message}
                </p>

                <button className="mt-5 bg-blue-500" type="submit" aria-disabled={pending}>
                    Submit
                </button>

            </form> */}
                <form className='flex flex-col h-full gap-3' onSubmit={actionSubmitLogin}>
                    <label>
                        Username
                        <input className='m-1 rounded-md text-gray-500' {...register('username')} type='text' placeholder='Username'  />
                    </label>

                    <label>                                
                        Password
                        <input className='m-1 rounded-md text-gray-500' {...register("password")}  type='password' placeholder='Password'  />
                    </label>

                    <button disabled={ isLoading } type='submit' className='bg-green-400 p-3 rounded-md w-28' >
                        {isLoading ? "Loading..." : "Save"}
                    </button>
                </form>

        </section>
    )
}
