

const initialState = {
    message: ""
}

export default function Register(){

    // const [ state, formAction ] = useFormState(login, initialState);

    // const { pending } = useFormStatus();

    return (
        <section className="grid place-items-center overflow-hidden h-screen">
            <form 
                // action={formAction} 
                className="flex flex-col items-left"
            >            
                <label className="text-left">Email</label>
                <input type="email" placeholder="Enter Email" className="rounded bg-['rgba(255, 255, 255, 0.06)']" name="email" />

                <label className="text-left mt-5">Create Username</label>
                <input type="text" placeholder="Create Username" className="rounded bg-['rgba(255, 255, 255, 0.06)']" name="password" />

                <label className="text-left mt-5">Create Password</label>
                <input type="password" placeholder="Create Password" className="rounded bg-['rgba(255, 255, 255, 0.06)']" name="password" />
                
                <label className="text-left mt-5">Confirm Password</label>                
                <input type="password" placeholder="Confirm Password" className="rounded bg-['rgba(255, 255, 255, 0.06)']" name="password" />
                <p aria-live="polite" className="sr-only" role="status">
                    {/* {state?.message} */}
                </p>

                <button 
                    className="mt-5 bg-blue-500" type="submit" 
                    // aria-disabled={pending}
                >
                    Submit
                </button>

            </form>
        </section>
    )
}
