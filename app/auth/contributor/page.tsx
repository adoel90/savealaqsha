"use client"

export default function ContributorPage (){

    return (
        <section className="flex flex-row justify-center">            
            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSduOrdEAD6sYOoVvn573RF_lVzHMEHlvJxiQaSLVuraijktnw/viewform?embedded=true" width="640"  className="h-screen" height="100%" frameBorder="0" marginHeight={0} marginWidth={0}>Memuat…</iframe>
        </section>
    )
}